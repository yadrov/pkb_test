export const mixin = {

    data(){
        return {
            items:[]
        }
    },
    methods:{
        async getData(params){
            const { data } = await axios.get('/api' + params)
            this.items = data.items 
        },
    },
    mounted(){
        this.getData(this.$route.path)

    },
    filters:{
        formatDate(str){
            const date = new Date(str)
            return date.toLocaleString('ru-Ru', {month:'long'}) + ' / ' + date.getFullYear()
        }
    },

}