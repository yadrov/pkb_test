import Router from 'vue-router'
import Vue from 'vue'
Vue.use(Router)

export default new Router({
    mode:'history',

    routes: [
        {
            path:'/',
            component: () => import('../components/Home')
        },
        {
            path: '/tables',
            component: () => import('../components/Tables'),
            children:[
                {
                    path:'people',
                    component: () => import('../components/Debtors')
                },
                {
                    path:'debts',
                    component: () => import('../components/Debts')
                },
                {
                    path:'debt/notpay',
                    component: () => import('../components/Debts/DebtsNotPay.vue')
                },
               
                {
                    path:'payments',
                    component: () => import('../components/Payments')
                },
                {
                    path:'portfolios',
                    component: () => import('../components/Portfolios'),
                    
                },
                {
                    path:'portfolio/efficiency',
                    component: () => import('../components/Portfolios/PortfoliosEfficiency'),

                },
                {
                    path:'portfolio/efficiency/bymonth',
                    component: () => import('../components/Portfolios/PortfoliosEfficiencyByMonth'),

                },
                {
                    path:'portfolio/bymonth',
                    component: () => import('../components/Portfolios/PortfolioByMonth'),

                },
                
            ]
        },
        
    ]
})