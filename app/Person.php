<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Debt;
use DB;

class Person extends Model
{
    
    protected $fillable = ['FIO'];
    public $timestamps = false;
/*    
 * 
 * all depstors with debts
 */
    public static function peoplesDebts($sum){
        return DB::table('debts')
            ->select([
                'people.FIO',
                DB::raw('SUM(debts.debt_sum) AS sum')
            ])
            ->leftJoin('people', 'debts.person_id', '=', 'people.id' )
            ->groupBy('FIO')
            ->havingRaw('SUM(debts.debt_sum) > '. $sum) 
            ->get();
    }
/**
 * 
 *  return depstor by id
 */
    public static function personDebts($id){
        return DB::table('people')
            ->select([
                'people.FIO',
                DB::raw('SUM(debt_sum) AS sum')
            ])
            ->leftJoin('debts', 'person_id', '=', 'people.id' )
            ->where('people.id', '=', $id)
            ->groupBy('FIO')
            ->get();
            
            
    }
/**
 * 
 * update person
 */
    // public function update($value){
    //     $this->FIO = $value;
    //     $this->save();
    // }

    
        
}
