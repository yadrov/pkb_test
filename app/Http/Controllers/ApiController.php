<?php

namespace App\Http\Controllers;
use DB;
use App\Person;
use App\Debt;
use App\Payment;
use App\Portfolio;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    // all tables
    public function getTable(Request $request){
        return ['items' => DB::select('SELECT * FROM '. $request->table)];
    }
/*
        filtered tables
*/ 

    public function getPersonDebts(Request $request){
        return DB::select('SELECT FIO, SUM(`debt_sum`) AS sum
            FROM debts
            LEFT JOIN people ON debts.person_id = people.id GROUP BY FIO HAVING SUM(debt_sum) > ' . $request->sum);
    }
    
    public function getPortfolioEfficiency(){
        return [
            'items' => DB::select('SELECT portfolios.name, req_debt_sum.debt_sum, IFNULL(req_payment_sum.payment_sum,0) AS payment_sum 
                FROM 
                (SELECT portfolio_id, sum(debt_sum) AS debt_sum 
                FROM debts 
                GROUP BY portfolio_id) AS req_debt_sum 
                LEFT JOIN
                (SELECT debts.portfolio_id, sum(payment_sum)  AS payment_sum 
                FROM
                payments
                LEFT JOIN 
                debts ON payments.debt_id = debts.id
                GROUP BY debts.portfolio_id) AS req_payment_sum ON req_debt_sum.portfolio_id = req_payment_sum.portfolio_id 
                LEFT JOIN portfolios ON req_debt_sum.portfolio_id = portfolios.id')
        ];
    }

    public function getPortfolioEfficiencyByMonth(){    
        return [
            'items' => DB::select('SELECT calendar.cal_date, req_debt.debt_sum, req_payment.payment_sum 
                FROM calendar
                LEFT JOIN (SELECT calendar.cal_date, IFNULL(sum(debt_sum),0) AS debt_sum
                FROM calendar 
                LEFT JOIN portfolios 
                ON calendar.cal_date <= end_date  and last_day(cal_date) >=  sign_date
                LEFT JOIN debts 
                ON debts.portfolio_id = portfolios.id 
                GROUP BY cal_date) AS req_debt
                ON calendar.cal_date = req_debt.cal_date
                LEFT JOIN (SELECT calendar.cal_date, IFNULL(sum(payment_sum),0) AS payment_sum
                FROM calendar 
                LEFT JOIN payments
                ON cal_date <= payments.payment_date and last_day(cal_date) >= payments.payment_date 
                GROUP BY cal_date) AS req_payment
                ON calendar.cal_date = req_payment.cal_date')
        ];
    }



   

    public function getPortfoloisByMonth(){
        return [
            'items' => DB::select('SELECT cal_date, sum(debt_sum) AS sum
                FROM
                calendar
                LEFT JOIN portfolios
                ON  cal_date <= end_date AND LAST_DAY(cal_date) >=  sign_date
                LEFT JOIN debts 
                ON portfolio_id = portfolios.id 
                GROUP BY cal_date 
                ORDER BY cal_date')
            ];
    }


    public function getDebtsWihoutPay(){
        return [
            'items' => 
                DB::select('SELECT DISTINCT debts.id, FIO, debt_sum
                FROM
                debts 
                JOIN people ON people.id = debts.person_id
                LEFT JOIN payments ON debts.id = payments.debt_id
                WHERE payments.debt_id IS NULL ')
        ];    
       
    }


    /**
     * update
     */

     public function updateDebtor(Request $request){
        DB::update('UPDATE people set FIO = ?  where people.id = ?', [$request->value, $request->id]);
     }
}
