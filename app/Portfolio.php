<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Portfolio extends Model
{   
    public static function portfoliosByMonth(){
        return DB::table('calendar')
        ->select([
            'cal_date',
            DB::raw('SUM(debt_sum)')
        ])
        ->leftJoin('portfolios', 
                DB::raw('cal_date >= sign_date AND LAST_DAY(cal_date) <= end_date'))
        ->leftJoin('debts', 'portfolio_id', '=',  'portfolios.id' )
        ->groupBy('cal_date')
        ->orderBy('cal_date')
        ->get();
    }

}
// select cal_date, sum(debt_sum)
// from
// calendar
// left join  portfolios
// on calendar.cal_date >= sign_date AND LAST_DAY(cal_date) <= end_date 
// left join debts 
// on portfolio_id = portfolios.id 
// group by cal_date 
// ORDER by cal_date