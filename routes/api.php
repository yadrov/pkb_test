<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** any table */
Route::get('tables/{table?}', 'ApiController@getTable'); 
/** должники */
Route::get('debtors/debts/{sum?}', 'ApiController@getPersonDebts'); //должники с суммой больше sum
Route::post('debtor', 'ApiController@updateDebtor');// изменение FIO

/** долги */
Route::get('tables/debt/notpay', 'ApiController@getDebtsWihoutPay');// долги без оплат


/** портфели */
Route::get('/tables/portfolio/efficiency', 'ApiController@getPortfolioEfficiency');  //эффективность портфелей
Route::get('/tables/portfolio/efficiency/bymonth', 'ApiController@getPortfolioEfficiencyByMonth');//' эффективность по месяцам
Route::get('/tables/portfolio/bymonth', 'ApiController@getPortfoloisByMonth');//портфели по месяцам





