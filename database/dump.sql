-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Сен 16 2019 г., 23:27
-- Версия сервера: 10.4.8-MariaDB-1:10.4.8+maria~disco
-- Версия PHP: 7.3.9-1+ubuntu19.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `PKB`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calendar`
--

CREATE TABLE `calendar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cal_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `calendar`
--

INSERT INTO `calendar` (`id`, `cal_date`, `created_at`, `updated_at`) VALUES
(1, '2010-04-01', NULL, NULL),
(2, '2010-05-01', NULL, NULL),
(3, '2010-06-01', NULL, NULL),
(4, '2010-07-01', NULL, NULL),
(5, '2010-08-01', NULL, NULL),
(6, '2010-09-01', NULL, NULL),
(7, '2010-10-01', NULL, NULL),
(8, '2010-11-01', NULL, NULL),
(9, '2010-12-01', NULL, NULL),
(10, '2011-01-01', NULL, NULL),
(11, '2011-02-01', NULL, NULL),
(12, '2011-03-01', NULL, NULL),
(13, '2011-04-01', NULL, NULL),
(14, '2011-05-01', NULL, NULL),
(15, '2011-06-01', NULL, NULL),
(16, '2011-07-01', NULL, NULL),
(17, '2011-08-01', NULL, NULL),
(18, '2011-09-01', NULL, NULL),
(19, '2011-10-01', NULL, NULL),
(20, '2011-11-01', NULL, NULL),
(21, '2011-12-01', NULL, NULL),
(22, '2012-01-01', NULL, NULL),
(23, '2012-02-01', NULL, NULL),
(24, '2012-03-01', NULL, NULL),
(25, '2012-04-01', NULL, NULL),
(26, '2012-05-01', NULL, NULL),
(27, '2012-06-01', NULL, NULL),
(28, '2012-07-01', NULL, NULL),
(29, '2012-08-01', NULL, NULL),
(30, '2012-09-01', NULL, NULL),
(31, '2012-10-01', NULL, NULL),
(32, '2012-11-01', NULL, NULL),
(33, '2012-12-01', NULL, NULL),
(34, '2013-01-01', NULL, NULL),
(35, '2013-02-01', NULL, NULL),
(36, '2013-03-01', NULL, NULL),
(37, '2013-04-01', NULL, NULL),
(38, '2013-05-01', NULL, NULL),
(39, '2013-06-01', NULL, NULL),
(40, '2013-07-01', NULL, NULL),
(41, '2013-08-01', NULL, NULL),
(42, '2013-09-01', NULL, NULL),
(43, '2013-10-01', NULL, NULL),
(44, '2013-11-01', NULL, NULL),
(45, '2013-12-01', NULL, NULL),
(46, '2014-01-01', NULL, NULL),
(47, '2014-02-01', NULL, NULL),
(48, '2014-03-01', NULL, NULL),
(49, '2014-04-01', NULL, NULL),
(50, '2014-05-01', NULL, NULL),
(51, '2014-06-01', NULL, NULL),
(52, '2014-07-01', NULL, NULL),
(53, '2014-08-01', NULL, NULL),
(54, '2014-09-01', NULL, NULL),
(55, '2014-10-01', NULL, NULL),
(56, '2014-11-01', NULL, NULL),
(57, '2014-12-01', NULL, NULL),
(58, '2015-01-01', NULL, NULL),
(59, '2015-02-01', NULL, NULL),
(60, '2015-03-01', NULL, NULL),
(61, '2015-04-01', NULL, NULL),
(62, '2015-05-01', NULL, NULL),
(63, '2015-06-01', NULL, NULL),
(64, '2015-07-01', NULL, NULL),
(65, '2015-08-01', NULL, NULL),
(66, '2015-09-01', NULL, NULL),
(67, '2015-10-01', NULL, NULL),
(68, '2015-11-01', NULL, NULL),
(69, '2015-12-01', NULL, NULL),
(70, '2016-01-01', NULL, NULL),
(71, '2016-02-01', NULL, NULL),
(72, '2016-03-01', NULL, NULL),
(73, '2016-04-01', NULL, NULL),
(74, '2016-05-01', NULL, NULL),
(75, '2016-06-01', NULL, NULL),
(76, '2016-07-01', NULL, NULL),
(77, '2016-08-01', NULL, NULL),
(78, '2016-09-01', NULL, NULL),
(79, '2016-10-01', NULL, NULL),
(80, '2016-11-01', NULL, NULL),
(81, '2016-12-01', NULL, NULL),
(82, '2017-01-01', NULL, NULL),
(83, '2017-02-01', NULL, NULL),
(84, '2017-03-01', NULL, NULL),
(85, '2017-04-01', NULL, NULL),
(86, '2017-05-01', NULL, NULL),
(87, '2017-06-01', NULL, NULL),
(88, '2017-07-01', NULL, NULL),
(89, '2017-08-01', NULL, NULL),
(90, '2017-09-01', NULL, NULL),
(91, '2017-10-01', NULL, NULL),
(92, '2017-11-01', NULL, NULL),
(93, '2017-12-01', NULL, NULL),
(94, '2018-01-01', NULL, NULL),
(95, '2018-02-01', NULL, NULL),
(96, '2018-03-01', NULL, NULL),
(97, '2018-04-01', NULL, NULL),
(98, '2018-05-01', NULL, NULL),
(99, '2018-06-01', NULL, NULL),
(100, '2018-07-01', NULL, NULL),
(101, '2018-08-01', NULL, NULL),
(102, '2018-09-01', NULL, NULL),
(103, '2018-10-01', NULL, NULL),
(104, '2018-11-01', NULL, NULL),
(105, '2018-12-01', NULL, NULL),
(106, '2019-01-01', NULL, NULL),
(107, '2019-02-01', NULL, NULL),
(108, '2019-03-01', NULL, NULL),
(109, '2019-04-01', NULL, NULL),
(110, '2019-05-01', NULL, NULL),
(111, '2019-06-01', NULL, NULL),
(112, '2019-07-01', NULL, NULL),
(113, '2019-08-01', NULL, NULL),
(114, '2019-09-01', NULL, NULL),
(115, '2019-10-01', NULL, NULL),
(116, '2019-11-01', NULL, NULL),
(117, '2019-12-01', NULL, NULL),
(118, '2020-01-01', NULL, NULL),
(119, '2020-02-01', NULL, NULL),
(120, '2020-03-01', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `debts`
--

CREATE TABLE `debts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `person_id` int(11) NOT NULL,
  `portfolio_id` int(11) NOT NULL,
  `debt_sum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `debts`
--

INSERT INTO `debts` (`id`, `person_id`, `portfolio_id`, `debt_sum`) VALUES
(1, 1, 1, 100),
(2, 1, 2, 200),
(3, 3, 4, 300),
(4, 4, 3, 400),
(9, 5, 5, 500),
(10, 2, 10, 50);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_09_11_125152_create_debts_table', 1),
(5, '2019_09_11_125225_create_portfolios_table', 1),
(6, '2019_09_11_125416_create_people_table', 1),
(7, '2019_09_11_125559_create_calendar_table', 1),
(8, '2019_09_11_130807_create_payments_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `debt_id` int(11) NOT NULL,
  `payment_sum` int(11) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`id`, `debt_id`, `payment_sum`, `payment_date`, `created_at`, `updated_at`) VALUES
(1, 1, 10, '2012-09-11', NULL, NULL),
(2, 1, 20, '2012-09-27', NULL, NULL),
(3, 4, 30, '2012-10-29', NULL, NULL),
(4, 3, 30, '2012-09-25', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `people`
--

CREATE TABLE `people` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `FIO` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `people`
--

INSERT INTO `people` (`id`, `FIO`) VALUES
(1, 'vb'),
(2, 'asdasd'),
(3, 'Ходорев В.Мa'),
(4, 'Васечкин А.Б'),
(5, 'Руккола');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolios`
--

CREATE TABLE `portfolios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign_date` date NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `portfolios`
--

INSERT INTO `portfolios` (`id`, `name`, `sign_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'PORTFOLIO_1', '2011-10-05', '2012-11-02', NULL, NULL),
(2, 'PORTFOLIO_2', '2010-09-01', '2012-10-01', NULL, NULL),
(3, 'PORTFOLIO_3', '2011-02-28', '2011-03-20', NULL, NULL),
(4, 'PORTFOLIO_4', '2010-08-15', '2011-09-12', NULL, NULL),
(5, 'PORTFOLIO_5', '2010-05-16', '2011-09-12', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `debts`
--
ALTER TABLE `debts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT для таблицы `debts`
--
ALTER TABLE `debts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `people`
--
ALTER TABLE `people`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT для таблицы `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
