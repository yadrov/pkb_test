<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        $date = new DateTime('2010-04-01');

        while($date < new DateTime('2017-04-01')){
            DB::table('calendar')-> insert([
                'cal_date' => $date
            ]);
            date_add($date, date_interval_create_from_date_string('1 month'));
        }

     
        // $this->call(UsersTableSeeder::class);
    }
}
